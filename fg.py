import cv2
import os
import sys


dataFolder = "VideoForCutting"

VideoFile = "video.mp4"

fileToOpen = os.path.join(dataFolder,VideoFile)


frame_counter = 0

cap = cv2.VideoCapture(fileToOpen)

ret, frame = cap.read()

if ret == False:
    print('********\n\nNot selected video\n\nVideo must be placed at "VideoForCutting/video.mp4"\n\n********')
    sys.exit(0)

height, width, _ = frame.shape

print("Size of full image: {}x{}".format(height, width))

print("Press \"Enter\"")
input()

#Main loop and calculatings
while 1:
    ret, frame = cap.read()
	
    if ret == False:
        print("ret == False")
        break
	
    print("Retrieved frame with number {}".format(frame_counter), end="\r")
	
    cv2.imwrite(os.path.join("frames","{}.png".format(frame_counter)),frame)
    
    frame_counter += 1
	
    #cv2.waitKey(27)

print("\nFinish")
cap.release()